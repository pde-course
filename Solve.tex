\chapter{Решение}
\section{Собственные функции}
Для последующих действий необходимо представить функцию $U$ из задачи \eqref{eq:new_problem} в виде разложения по собственным функциям. Сначала найдем собственные функции. Для этого решим задачу Штурма-Лиувилля. Представим функцию $U$ в виде
\begin{equation}
  \label{func:form}
  U(y, z, t) = T(t)Y(y)Z(z).
\end{equation}

Рассмотрим однородное уравнение, соответствующее неоднородному уравнению из задачи \eqref{eq:new_problem}:
\begin{equation}
  \label{eq:uniform}
  \frac{1}{c^2} \frac{\partial^2 U}{\partial t^2} = \Delta_{yz} U.\\
\end{equation}

Подставив \eqref{func:form} в \eqref{eq:uniform}, получим
\begin{equation}
  \label{eq:equality}
  \frac{1}{c^2}\frac{T''}{T} = \frac{Y''}{Y} + \frac{Z''}{Z} = -(\nu^2 + \xi^2).\\
\end{equation}

Здесь $\nu = \const, \xi = \const$ в силу того, что левая часть \eqref{eq:equality} зависит только от $t$,
а правая~--- от $y$ и $z$. Таким образом,
\begin{equation}
  \label{eq:shturm-liuville}
  \left.
  \begin{array}{rcl}
    Y'' + \nu^2 Y &=& 0,\\
    Z'' + \xi^2 Z &=& 0,\\
    T'' + c^2(\nu^2 + \xi^2)T &=& 0.
  \end{array}
  \right.
\end{equation}

Подставив, кроме того, \eqref{func:form} в граничные условия задачи \eqref{eq:new_problem}, получим условия
\begin{equation}
  \left.
  \label{eq:conditions}
  \begin{array}{rcl}
    Y(0) &=& 0,\qquad Y(l_y) = 0,\\
    Z(0) &=& 0,\qquad Z(l_z) = 0.
  \end{array}
  \right.
\end{equation}

Объединяя \eqref{eq:shturm-liuville} и \eqref{eq:conditions}, мы получим две задачи о собственных значениях (задачи Штурма-Лиувилля) для $Y$ и $Z$.

\begin{equation}
  \label{eq:shturm-liuville-final}
  \begin{array}{ll}
    \left\{
      \begin{array}{rcl}
        Y'' + \nu^2Y & = & 0, \\
        Y(0) & = & 0, \\
        Y(l_y) & = & 0;
      \end{array}
    \right.
    \left\{
      \begin{array}{rcl}
        Z'' + \xi^2Z & = & 0, \\
        Z(0) & = & 0, \\
        Z(l_z) & = & 0.
      \end{array}
    \right.  
  \end{array}
\end{equation}

Решим первую из задач \eqref{eq:shturm-liuville-final}. Как известно (например, из \cite{samarsky}), общее решение такого уравнения представимо в виде
\begin{equation}
  \label{eq:common-solution}
  Y(y) = A\sin{\nu y} + B\cos{\nu y}.
\end{equation}

Первое граничное условие $Y(0) = 0$ дает нам $B = 0$. Из второго условия $Y(l_y) = 0$ следует 
\[
Y(l_y) = A\sin{\nu l_y} = 0.
\]

Поскольку $Y(y)$ не равно тождественно нулю, то $A \ne 0$, значит
\begin{equation}
  \label{eq:sin}
  \sin{\nu l_y} = 0.
\end{equation}

Из \eqref{eq:sin} следует, что $\nu = \cfrac{\pi n}{l_y}$, где $n \in \Z$. Собственные функции \eqref{eq:common-solution} при этом имеют вид $Y = A \sin \cfrac{\pi n}{l_y}$.

Аналогично решаем вторую из задач \eqref{eq:shturm-liuville-final} и получаем нетривиальные решения
\begin{equation}
  \label{func:eigen}
  \begin{array}{ll}
    \left\{
      \begin{array}{l}
        Y = A \sin\nu y, \\
        \nu = \frac{\pi n}{l_y}, n \in \Z;
      \end{array}
    \right.
    \left\{
      \begin{array}{l}
        Z = C \sin\xi z, \\
        \xi = \frac{\pi m}{l_z}, m \in \Z.
      \end{array}
    \right.  
  \end{array}
\end{equation}

Здесь понимается, что для любых вещественных констант $A$ и $C$, указанные $Y(y)$ и $Z(z)$ представляют собой искомые собственные функции.

\section{Получение решения}

Таким образом, $U$ можно представить в виде следующего двойного ряда Фурье по функциям \eqref{func:eigen}.
\begin{equation}
  \label{eq:u-series}
  U(y, z, t) = \displaystyle \sum_{m=1}^{\infty}\sum_{n=1}^{\infty} \gamma(t) \sin\frac{\pi n y}{l_y} \sin\frac{\pi m z}{l_z}.
\end{equation}

Разложим по собственным функциям \eqref{func:eigen} неоднородную правую часть \eqref{eq:g} дифференциального уравнения задачи \eqref{eq:new_problem} и функцию \eqref{eq:phi} из начальных условий этой задачи.

\begin{enumerate}
\item Разложение $G(y, z, t)$.
  Разложим функцию \eqref{eq:g} по функциям \eqref{func:eigen}. Заметим, что функция $G$ уже содержит в себе собственную функцию $\sin\frac{\pi y}{l_y}$, значит, необходимо разложить лишь зависящую от $z$ часть. Разложение будет иметь вид
  \begin{equation}
    \label{func:G_represent}
    G(y, z, t) =  \pi^2 \frac{4l_y^2 - \lambda^2}{\lambda^2 \l_y^2} \sin\frac{2\pi c}{\lambda}t\displaystyle \sum_{m=1}^{\infty}  g_{nm}^{(z)}(t) \sin\frac{\pi y}{l_y} \sin\frac{\pi m z}{l_z}.
  \end{equation}

  Найдем коэффициенты ряда.

  \begin{eqnarray}
    \nonumber
    g^{(z)}_{nm}(t) &=& \frac{2}{l_z} \displaystyle \int_0^{l_z} \frac{l_z - z}{l_z} \sin\frac{\pi m z}{l_z} dz = \frac{2}{l_z}\left[\int_0^{l_z}\sin\frac{\pi m z}{l_z}dz - \int_0^{l_z}\frac{z}{l_z} \sin\frac{\pi m z}{l_z}dz\right] =\\
    \nonumber
    &=& \frac{2}{l_z} \left[ \left. -\frac{l_z}{\pi m} \cos\frac{\pi m z}{l_z}\right|_{0}^{l_z} - \left. \frac{z l_z}{\pi m}\cos\frac{\pi m z}{l_z}\right|_{0}^{l_z} + \int_0^{l_z}\frac{l_z}{\pi m}\cos\frac{\pi m z}{l_z}dz\right] =\\
    \label{func:g}
    &=& \frac{2}{l_z}\frac{l_z}{\pi m} \left( 1 - (-1)^m - (-1)^{m+1} \right) = \frac{2}{\pi m}.
  \end{eqnarray}
  
  Подставив разложение \eqref{func:g} в \eqref{func:G_represent}, получим
  \begin{equation}
    \label{func:G}
    G(y, z, t) = 2\pi \frac{4l_y^2 - \lambda^2}{\lambda^2 \l_y^2} \sin\frac{2\pi c}{\lambda}t\displaystyle \sum_{m=1}^{\infty}\frac{1}{m}\sin\frac{\pi y}{l_y} \sin\frac{\pi m z}{l_z}.
  \end{equation}

\item Разложение $\Phi(y, z)$.

  Теперь разложим функцию \eqref{eq:phi}, содержащуюся в начальном условии задачи \eqref{eq:new_problem}. Как и в предыдущем случае, она уже разложена по собственным функциям относительно $y$, будем искать разложение относительно $sin\frac{\pi m z}{l_z}$. Разложение будет иметь следующий вид
  \[
  \Phi(y, z) = \displaystyle -k\sum_{m=1}^{\infty}\varphi \sin\frac{\pi y}{l_y} \sin\frac{\pi m z}{l_z}.
  \]
  
  Найдем коэффициенты разложения.
  \begin{eqnarray*}
    \varphi = \displaystyle \frac{2}{l_z} \int_0^{l_z} \frac{l_z - z}{l_z}\sin\frac{\pi m z}{l_z}dz = \frac{2}{\pi m}.
  \end{eqnarray*}
  
  Таким образом,
\begin{equation}
\label{func:phi}
  \Phi(y, z) = -\frac{2k}{\pi}\displaystyle \sum_{m=1}^{\infty} \frac{1}{m} \sin\frac{\pi y}{l_y} \sin\frac{\pi m z}{l_z}.
\end{equation}

\end{enumerate}

Подставив \eqref{func:G}, \eqref{func:phi} и \eqref{eq:u-series} в \eqref{eq:new_problem}, умножив всё на $c^2$ и приравняв коэффициенты при одинаковых базисных функциях, получим задачу Коши.
\begin{equation}
\label{eq:cauchy}
\left\{
    \begin{array}{l}
      \gamma''(t) + w^2_{m}\gamma(t) = \frac{2\pi c^2}{m}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt}, ~ 0 \le t \le T; \\
      \begin{array}{rcl}
      \gamma(0) &=& 0,\\
      \gamma'(0) &=& -\frac{2k}{\pi m},
      \end{array}
    \end{array}
\right.
\end{equation}
где $w_{m} = \pi c \sqrt{\frac{1}{l_y^2} + \frac{m^2}{l_z^2}}$.

Здесь $U$ представляется в виде обычного ряда Фурье, а не двойного, потому что все коэффициенты в \eqref{func:G} и \eqref{func:phi} равны нулю, если $n \neq 1$.

Сначала найдём общее решение соответствующего однородного уравнения
\begin{equation}
\label{eq:similar}
\begin{array}{l}
\gamma''(t) + w^2_{m}\gamma(t) = 0.
\end{array}
\end{equation}

Решение \eqref{eq:similar} не составляет труда: это обыкновенное линейное дифференциальное уравнение с постоянными коэффициентами, характерное для линейных осцилляторов. Его решение

\begin{equation}
\label{eq:similar_solution}
\gamma^{0}(t) = C_1 \cos{w_m t} + C_2 \sin{w_m t}, ~ C_1, C_2 \in \R.
\end{equation}

Рассмотрим теперь неоднородную задачу и найдем ее частное решение. Исходя из вида правой части, вид решения будет таким
\begin{equation}
\label{eq:part}
\tilde{\gamma}(t) = D_1 \cos{kt} + D_2 \sin{kt}.
\end{equation}

Подставим функцию \eqref{eq:part} в уравнение задачи \eqref{eq:cauchy} и получим
\[
-k^2 D_1 \cos{kt} - k^2 D_2 \sin{kt} + w^2_m D_1 \cos{kt} + w^2_{m} D_2 \cos{kt} = \frac{2\pi c^2}{m}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt}.
\]

Приравняем коэффициенты при соответствующих функциях и получим систему
\begin{equation}
\label{eq:sys_temp}
\begin{equationsset}
      (w_m^2 - k^2) D_1 & = & 0, \\
      (w_m^2 - k^2) D_2 & = & \frac{2\pi c^2}{m}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right).
\end{equationsset}
\end{equation}

Отсюда $D_1 = 0$, $D_2 = \frac{2\pi c^2}{m(w_m^2 - k^2)}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)$. Получаем искомое частное решение.

\begin{equation}
\label{eq:part_solution}
\tilde{\gamma}(t) = \frac{2\pi c^2}{m(w_m^2 - k^2)}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt}.
\end{equation}

Общее решение \eqref{eq:cauchy} выглядит как сумма общего решения \eqref{eq:similar_solution} соответствующего однородного уравнения и частного решения \eqref{eq:part_solution} его самого, то есть

\begin{equation}
\label{eq:cauchy_general_solution}
\gamma(t) = C_1 \cos{w_mt} + C_2 \sin{w_mt} + \frac{2\pi c^2}{m(w_m^2 - k^2)}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt}.
\end{equation}

Используем начальные условия и получим
\begin{equation}
\label{eq:sys}
\left\{
  \begin{array}{l}
    C_1 = 0,\\
    C_2 = -\frac{k}{w_m}\left(\frac{2}{\pi m} + D \right).
  \end{array}
\right.
\end{equation}

Таким образом, учитывая \eqref{eq:sys} в \eqref{eq:cauchy_general_solution}, получим
\begin{equation}
\label{eq:cauchy_solution}
  \gamma(t) = \frac{2\pi c^2}{m(w_m^2 - k^2)}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt} - \frac{k}{w_m}\left(\frac{2}{\pi m} + D\right)\sin{w_mt}.
\end{equation}

Тогда с учётом \eqref{eq:cauchy_solution} \eqref{eq:u-series} запишется так:
\begin{multline}
\label{eq:u_solution}
  U(y, z, t) = \displaystyle \sum_{m=1}^{\infty} \left( \frac{2\pi c^2}{m(w_m^2 - k^2)}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt} - \frac{k}{w_m}\left(\frac{2}{\pi m} + D\right)\sin{w_mt} \right) \sin\frac{\pi y}{l_y} \sin\frac{\pi m z}{l_z}.
\end{multline}

Окончательно, выразив $E_x$ из \eqref{eq:changeling}, и подставив туда \eqref{eq:u_solution}, имеем
\begin{multline}
\label{eq:ultimate_solution}
  E_x(y, z, t) = \displaystyle
    \sum_{m=1}^{\infty} \left( \frac{2\pi c^2}{m(w_m^2 - k^2)}\left(\frac{4l_y^2 - \lambda^2}{l_y^2\lambda^2} \right)\sin{kt} - \frac{k}{w_m}\left(\frac{2}{\pi m} + D\right)\sin{w_mt} \right) \sin\frac{\pi y}{l_y} \sin\frac{\pi m z}{l_z}+\\
    + \frac{l_z - z}{l_z} \sin\frac{2\pi c}{\lambda}t \sin\frac{\pi y}{l_y}.
\end{multline}

Это и есть окончательное аналитическое решение исходной задачи \eqref{eq:problem} в виде ряда Фурье. На рисунках \ref{pic:first}, \ref{pic:second} и \ref{pic:third} приведена иллюстрация данного решения с использованием приведенных параметров \eqref{eq:params} для фиксированного $y = \cfrac{l_y}{2}$ и для различных временных моментов $t$.

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=0.8\linewidth]{first}
  \caption{Волна в момент $t = 1\cdot10^{-14}$ с.}
  \label{pic:first}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=0.8\linewidth]{second}
  \caption{Волна в момент $t = 5\cdot10^{-14}$ с.}
  \label{pic:second}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=0.8\linewidth]{third}
  \caption{Волна в момент $t = 1\cdot10^{-13}$ с.}
  \label{pic:third}
\end{figure}
